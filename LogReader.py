import csv
import pickle
import os
import time

from User import User
from Traces.Operation import *
from Traces.OperationTransformer import transformOperation
from datetime import datetime

if __name__ == "__main__" :

    maxFileAge = 600 # 600 seconds, 10 minutes

    needToCreateNewFile = True # not os.path.isfile('./users.pickle')

    if not needToCreateNewFile:
        print("Backup found")
        if time.time()-os.stat("./users.pickle").st_mtime < maxFileAge:
            print("Loading from backup")
            with open('users.pickle', 'rb') as backupfile:
                users = pickle.load(backupfile)
        else:
            print("Backup too old")
            needToCreateNewFile = True

    if needToCreateNewFile:
        print("Creating new file")
        unknowns = 0
        users = {}
        with open("ludimoodle_trace.log", 'r') as file:
            csvReader = csv.reader(file, delimiter=",")# delimiter = ";"

            for row in csvReader:
                tmp = User.User(row[1].strip(), row[2])

                if tmp.name not in users:
                    users[tmp.name] = tmp
                else:
                    users[tmp.name].addGameElement(row[2])

                user = users[tmp.name]

                op = buildOperation(row, user)

                if type(op) != Operation:
                    user.addOperation(op)

                # if type(op) == Operation:
                #     op.addOpType(row[3])
                #     unknowns += 1

        with open('users.pickle', 'wb') as backupfile:
            pickle.dump(users, backupfile, protocol=pickle.HIGHEST_PROTOCOL)

    print("Created new user timeline")


    selectedUser : User = users["score.stuart@univ-lyon3.fr"]

    todayTimeline = selectedUser.operationsPerformed

    #for timestamp in sorted(todayTimeline.keys()):
    #    print(todayTimeline[timestamp])

    transformedTimeline = transformOperation(todayTimeline)

    selectedUser.observations = transformedTimeline

    for action in transformedTimeline:
        print(action)
