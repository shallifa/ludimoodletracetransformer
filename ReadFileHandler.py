from watchdog.events import FileSystemEventHandler
import os

class FileReaderHandler(FileSystemEventHandler):

    currentLine = 0

    def on_modified(self, event):
        print('File has been modified')
        print(event.src_path)

        with open(event.src_path, 'rb') as f:
            f.seek(-2, os.SEEK_END)
            while f.read(1) != b'\n':
                f.seek(-2, os.SEEK_CUR)
            print(f.readline().decode())

