class HexadScores:
    @classmethod
    def fromQuestionnaire(cls, questionnaireScoreList):
        print("from questionnaireScores not yet implemented")
        return HexadScores(0,0,0,0,0,0)

    def __init__(self, achieverScore, playerScore, socialiserScore, freeSpiritScore, disruptorScore, philanthropistScore):
        self.achiever = achieverScore
        self.player = playerScore
        self.socialiser = socialiserScore
        self.freeSpirit = freeSpiritScore
        self.disruptor = disruptorScore
        self.philanthropist = philanthropistScore


class MotivationScores:
    @classmethod
    def fromQuestionnaire(cls, questionnaireScoreList):
        print("method not yet implemented")
        return MotivationScores()

    # Types de motivation
    # Intrinseque :
    #     MICO : a la connaissance
    #     MIAC : a l'accomplissement
    #     MIST : a la simulation
    # Extrinseque :
    #     MEID : a la regulation identifiee
    #     MEIN : a la regulation introjectee
    #     MERE : a la regulation externe
    # Amotivation :
    #     AMOT : amotivation

    def __init__(self, mico, miac, mist, meid, mein, mere, amot):
        self.mico = mico
        self.miac = miac
        self.mist = mist
        self.meid = meid
        self.mein = mein
        self.mere = mere
        self.amot = amot

    def variation(self, motivationScores):
        return MotivationScores(motivationScores.mico - self.mico, motivationScores.miac - self.miac, motivationScores.mist - self.mist, motivationScores.meid - self.meid, motivationScores.mein - self.mein, motivationScores.mere - self.mere, motivationScores.amot - self.amot)

class UserProfileData:
    def __init__(self, hexadScores, age, gender, initialMotivationScores):
        self.hexadScores = hexadScores
        self.age = age
        self.gender = gender
        self.initialMotivationScores = initialMotivationScores