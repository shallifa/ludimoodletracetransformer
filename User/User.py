import User.UserProfileData as upd
from Traces.Observation import *

class User:
    def __init__(self, name, gameElement, userProfileData = upd.UserProfileData(0,0,0,0)):
        self.name = name
        self.gameElements = [gameElement]
        self.currentGameElement = gameElement
        self.operationsPerformed = {}
        self.observations = {}
        self.userProfile = userProfileData

    def addGameElement(self, gameElement):
        if gameElement not in self.gameElements:
            self.gameElements.append(gameElement)

        self.currentGameElement = gameElement

    def numberOfGameElements(self):
        return len(self.gameElements)

    def addOperation(self, operation):
        self.operationsPerformed[operation.timestamp] = operation

    def printTimeline(self):
        for timestamp in sorted(self.operationsPerformed.keys()):
            print(self.operationsPerformed[timestamp])

    def getTimeline(self):
        return self.operationsPerformed

    def getTimelineFromDate(self, date):
        tmp = {}
        for timestamp in self.operationsPerformed:
           if timestamp.date() == date.date():
               tmp[timestamp] = self.operationsPerformed[timestamp]

        return tmp

    def getNumberOfPauses(self):
        i = 0
        for timestamp in self.observations :
            if type(self.observations[timestamp]) is Pause:
                i+=1
        return i

    def exportObservations(self):
        output = "Action;User;GameElement;Debut;Fin;Time since last update;Infos\n"

        with open("transformedTrace.csv", 'w') as file:
            for observation in self.observations:
                output += str(observation) + "\n"

            file.write(output)


    def __eq__(self, other):
        return self.name == other.name