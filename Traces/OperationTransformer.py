from Traces.Operation import *
from Traces.Observation import *
from datetime import timedelta

# STATIC VALUES
pauseDelta = timedelta(minutes=2)
passRate = 0.7 #The % of correct answers to consider that a quiz is successful

def transformOperation(timeline):

    # The transformed timeline that contains the start timestamp and corresponding Action
    transformedTimeline = []

    # VARIABLES USED
    attemptNumber = ""
    quizStart = ''
    inQuiz = False
    interruptedQuiz = False
    numberOfQuestions = 0.0
    correctQuestions = 0.0
    previousQuizzes = {}
    success = ''
    lastUpdate = None

    timestamps = sorted(timeline.keys())
    i = 0



    while i< len(timestamps):
        #TODO : Ajouter la transformation pour visiter la page X pendant un quiz

        # Get current operation
        op : Operation = timeline[timestamps[i]]

        if type(op) is GameElementUpdate:
            lastUpdate = op.timestamp
            i = i+1
            continue

        if not inQuiz:
            if type(op) in [QuizPageview]:
                transformedTimeline.append(ResumeQuizAttempt(op.timestamp, op.timestamp, op.user, op.gameElement, op.quizId, op.attemptNumber, lastUpdate))
                quizStart = op
                inQuiz = True
                interruptedQuiz = True

                try:
                    numberOfQuestions = previousQuizzes[quizStart.quizId][1]
                    correctQuestions = previousQuizzes[quizStart.quizId][2]
                except KeyError:
                    print("No previous info about this past quiz")
                    numberOfQuestions = 0.0
                    correctQuestions = 0.0
                    success = "Unsure"

                    previousQuizzes[quizStart.quizId] = [False, numberOfQuestions, correctQuestions]

        if i != 0:
            previousOp = timeline[timestamps[i-1]]

            if inQuiz:
                if type(op) not in [QuizPageview, QuestionGradedRight, QuestionGradedWrong, QuizAttemptFinished]:
                    interruptedQuiz = True

                if (type(op) is QuizPageview) and (op.quizId != quizStart.quizId):
                    transformedTimeline.append(AbandonQuiz(op.timestamp, op.timestamp, op.user, op.gameElement, quizStart.quizId, quizStart.attemptNumber, numberOfQuestions, correctQuestions, lastUpdate))
                    transformedTimeline.append(ResumeQuizAttempt(op.timestamp, op.timestamp, op.user, op.gameElement, op.quizId, op.attemptNumber, lastUpdate))
                    quizStart = op
                    inQuiz = True
                    interruptedQuiz = True

                    try:
                        numberOfQuestions = previousQuizzes[quizStart.quizId][1]
                        correctQuestions = previousQuizzes[quizStart.quizId][2]
                    except KeyError:
                        print("No previous info about this past quiz")
                        numberOfQuestions = 0.0
                        correctQuestions = 0.0
                        success = "Unsure"

                        previousQuizzes[quizStart.quizId] = [False, numberOfQuestions, correctQuestions]

                    i+=1
                    continue


            # Pause check
            if timestamps[i] - timestamps[i-1] >= pauseDelta:
                transformedTimeline.append(Pause(timestamps[i-1], timestamps[i], op.user, op.gameElement, previousOp, op, lastUpdate))
                i+=1
                continue

            # Attempt quiz check
            if (type(op) is QuizAttemptStarted) and (type(previousOp) is QuizModuleview):
                #if op.quizId == previousOp.quizId: #This check doesn't currently work as ids don't match !
                if inQuiz:
                    transformedTimeline.append(AbandonQuiz(previousOp.timestamp, op.timestamp, op.user, op.gameElement, quizStart.quizId, quizStart.attemptNumber, numberOfQuestions, correctQuestions, lastUpdate))

                if op.quizId not in previousQuizzes.keys():
                    transformedTimeline.append(AttemptQuiz(previousOp.timestamp, op.timestamp, op.user, op.gameElement, op.quizId, op.attemptNumber, lastUpdate))
                else:
                    transformedTimeline.append(RestartQuiz(previousOp.timestamp, op.timestamp, op.user, op.gameElement, op.quizId, op.attemptNumber, lastUpdate, previousQuizzes[op.quizId], lastUpdate))

                quizStart = op
                inQuiz = True
                interruptedQuiz = False
                numberOfQuestions = 0.0
                correctQuestions = 0.0
                success = False

                previousQuizzes[quizStart.quizId] = [False, numberOfQuestions, correctQuestions]


                i+=1
                continue

            # Complete question check
            if (type(op) in [QuestionGradedRight, QuestionGradedWrong]) and (type(previousOp) is QuizPageview):
                if op.quizId == previousOp.quizId:
                    transformedTimeline.append(CompleteQuestion(previousOp.timestamp, op.timestamp, op.user, op.gameElement, lastUpdate, type(op) is QuestionGradedRight))
                i+=1
                numberOfQuestions += 1

                if type(op) is QuestionGradedRight:
                    correctQuestions += 1

                previousQuizzes[quizStart.quizId][1] = numberOfQuestions
                previousQuizzes[quizStart.quizId][2] = correctQuestions
                continue

            #Complete quiz check
            if type(op) is QuizAttemptFinished:
                if success != "Unsure" :
                    success = correctQuestions/numberOfQuestions > passRate

                transformedTimeline.append(CompleteQuiz(quizStart.timestamp, op.timestamp, op.user, op.gameElement, op.quizId, op.attemptNumber, lastUpdate, interruptedQuiz, success))
                inQuiz = False
                interruptedQuiz = False

                previousQuizzes[op.quizId] = [success, numberOfQuestions, correctQuestions]

                numberOfQuestions = 0.0
                correctQuestions = 0.0

        i+=1


    return transformedTimeline