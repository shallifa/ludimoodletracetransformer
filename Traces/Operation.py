from datetime import datetime
from User import User

def buildOperation(logRow, user):
    #Row structure
    timestamp = logRow[0].strip()
    gameElement = logRow[2].strip()
    op = logRow[3].strip()

    if len(logRow) > 4:
        parameters = logRow[4:]

    #General operations
    if op == "login":
        return Login(timestamp, user, gameElement)
    elif op == "course_pageview" :
        # TODO : Ajouter la condition ou on a coursesectionumber : X en position 2 de parametre. Creer classe SectionPageview(...)

        return CoursePageview(timestamp, user, gameElement, parameters[0])
    elif op == "feature_change" : #Not yet implemented
        return FeatureChange(timestamp, user, gameElement)
    elif op == "dashboard" :
        return Dashboard(timestamp, user, gameElement)

    #Quiz related operations
    elif op == "quiz_moduleview":
        return QuizModuleview(timestamp, user, gameElement, parameters[1])
    elif op == "quiz_attempt_started" :
        return QuizAttemptStarted(timestamp, user, gameElement, parameters[0], parameters[1], parameters[2])
    elif op == "quiz_attempt_finished" :
        return QuizAttemptFinished(timestamp, user, gameElement, parameters[0], parameters[1], parameters[2])
    #elif op == "quiz_start":
    #    return QuizAttemptStarted(timestamp, user, gameElement, parameters[0], 1)
    elif op == "quiz_pageview":
        return QuizPageview(timestamp, user, gameElement, parameters[1], parameters[2], parameters[3])
    elif op == "question_gradedright":
        return QuestionGradedRight(timestamp, user, gameElement, parameters[3], parameters[0], parameters[2])
    elif op == "question_gradedwrong":
        return QuestionGradedWrong(timestamp, user, gameElement, parameters[3], parameters[0], parameters[2])
    elif op == "quiz_review":
        return QuizReview(timestamp, user, gameElement, parameters[0], parameters[1])
    elif op == "quiz_submit":
        return QuizSubmit(timestamp, user, gameElement, parameters[0], parameters[1])
    elif op == "quiz_results": #Not yet active
        return QuizResults(timestamp, user, gameElement, parameters[0], parameters[1])

    #Game element related trace
    elif "_update" in op:
        return GameElementUpdate(timestamp, user, gameElement)

    #TODO: Les Operations Visiter Calendrier, Page de prefs, page de profil

    else:
        return Operation(timestamp, user, gameElement)

class Operation:
    def __init__(self, timestamp, user, gameElement):
        self.timestamp = datetime.utcfromtimestamp(int(timestamp))
        self.user = user
        self.gameElement = gameElement

    def print(self):
        return "Simple Operation"

    def addOpType(self, opType):
        self.opType = opType

    def __str__(self):
        return "Unknown operation :" + self.timestamp.strftime('%Y-%m-%d %H:%M:%S') + " " + self.opType


class Login(Operation):
    def __init__(self, timestamp, user, gameElement):
        super().__init__(timestamp, user, gameElement)

    def __str__(self):
        return "Login :" + self.timestamp.strftime('%Y-%m-%d %H:%M:%S')

class Dashboard(Operation):
    def __init__(self, timestamp, user, gameElement):
        super().__init__(timestamp, user, gameElement)

    def __str__(self):
        return "Dashboard :" + self.timestamp.strftime('%Y-%m-%d %H:%M:%S')

class CoursePageview(Operation):
    def __init__(self, timestamp, user, gameElement, page):
        super().__init__(timestamp, user, gameElement)
        self.page = page

    def __str__(self):
        return "Pageview :" + self.timestamp.strftime('%Y-%m-%d %H:%M:%S') + " page : " + self.page

class FeatureChange(Operation):
    def __init__(self , timestamp, user, gameElement):
        super().__init__(timestamp, user, gameElement)

    def __str__(self):
        return "FeatureChange :" + self.timestamp.strftime('%Y-%m-%d %H:%M:%S')

class QuizModuleview(Operation):
    def __init__(self, timestamp, user, gameElement, quizId):
        super().__init__(timestamp, user, gameElement)
        self.quizId = quizId.strip()

    def __str__(self):
        return "Quiz module view :" + self.timestamp.strftime('%Y-%m-%d %H:%M:%S') + self.quizId

class QuizAttemptStarted(Operation):
    def __init__(self, timestamp, user, gameElement, attemptNumber, course, quizId):
        super().__init__(timestamp, user, gameElement)
        self.quizId = quizId.strip()
        self.attemptNumber = attemptNumber
        self.course = course

    def __str__(self):
        return "Quiz attempt started :" + self.timestamp.strftime('%Y-%m-%d %H:%M:%S') + self.quizId + str(self.attemptNumber)

class QuizAttemptFinished(Operation):
    def __init__(self, timestamp, user, gameElement, attemptNumber, course, quizId):
        super().__init__(timestamp, user, gameElement)
        self.quizId = quizId.strip()
        self.attemptNumber = attemptNumber
        self.course = course

    def __str__(self):
        return "Quiz attempt finished:" + self.timestamp.strftime('%Y-%m-%d %H:%M:%S') + self.quizId + str(self.attemptNumber)


class QuizPageview(Operation):
    def __init__(self, timestamp, user, gameElement, quizId, attemptNumber, pageNumber):
        super().__init__(timestamp, user, gameElement)
        self.quizId = quizId.strip()
        self.attemptNumber = attemptNumber
        self.pageNumber = pageNumber

    def __str__(self):
        return "Quiz page view :" + self.timestamp.strftime('%Y-%m-%d %H:%M:%S') + self.quizId + self.attemptNumber + self.pageNumber

class QuestionGradedRight(Operation):
    def __init__(self, timestamp, user, gameElement, quizId, attemptNumber, pageNumber):
        super().__init__(timestamp, user, gameElement)
        self.quizId = quizId.strip()
        self.attemptNumber = attemptNumber.strip()
        self.pageNumber = pageNumber.strip()

    def __str__(self):
        return "Question graded right :" + self.timestamp.strftime('%Y-%m-%d %H:%M:%S') + self.quizId + self.attemptNumber + self.pageNumber

class QuestionGradedWrong(Operation):
    def __init__(self, timestamp, user, gameElement, quizId, attemptNumber, pageNumber):
        super().__init__(timestamp, user, gameElement)
        self.quizId = quizId.strip()
        self.attemptNumber = attemptNumber.strip()
        self.pageNumber = pageNumber.strip()

    def __str__(self):
        return "Question graded right :" + self.timestamp.strftime('%Y-%m-%d %H:%M:%S') + self.quizId + self.attemptNumber + self.pageNumber

class QuizReview(Operation):
    def __init__(self, timestamp, user, gameElement, quizId, attemptNumber):
        super().__init__(timestamp, user, gameElement)
        self.quizId = quizId
        self.attemptNumber = attemptNumber

    def __str__(self):
        return "Quiz review :" + self.timestamp.strftime('%Y-%m-%d %H:%M:%S') + self.quizId + self.attemptNumber

class QuizSubmit(Operation):
    def __init__(self, timestamp, user, gameElement, quizId, attemptNumber):
        super().__init__(timestamp, user, gameElement)
        self.quizId = quizId
        self.attemptNumber = attemptNumber

    def __str__(self):
        return "Quiz submit :" + self.timestamp.strftime('%Y-%m-%d %H:%M:%S') + self.quizId + self.attemptNumber

class QuizResults(Operation):
    def __init__(self, timestamp, user, gameElement, quizId, attemptNumber, quizResults):
        super().__init__(timestamp, user, gameElement)
        self.quizId = quizId
        self.attemptNumber = attemptNumber
        self.quizResults = quizResults

    def __str__(self):
        return "Quiz results :" + self.timestamp.strftime('%Y-%m-%d %H:%M:%S') + self.quizId + self.attemptNumber + self.quizResults

class GameElementUpdate(Operation):
    def __init__(self, timestamp: datetime, user: User, gameElement: str):
        """

        :param timestamp:
        :param user:
        :param gameElement:
        """
        super().__init__(timestamp, user, gameElement)

    def __str__(self):
        return "Game element update : " + self.timestamp.strftime('%Y-%m-%d %H:%M:%S')