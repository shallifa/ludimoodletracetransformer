from datetime import datetime
from datetime import timedelta
from User import User
from typing import NewType
from Traces import Operation

GameElement = NewType("GameElement", str)

#TODO: Renommer les classes en "Observation" - changer le nom du fichier (refactor)
#TODO : Créer une classe remonter depuis un quiz avec un attribut qui donne jusqu'ou on remonte (section, course). Pour les Observations Visiter la page X pendant un quiz



class Observation:
    def __init__(self, start: datetime, end: datetime, user: User, gameElement: GameElement, previousUpdate: datetime):
        """

        :param start: A datetime at the start of the Action
        :param end:  A date time at the end of the Action
        :param user: The user that performed the Action
        :param gameElement: The game element that the user had when the performed the Action
        """
        self.start = start
        self.end = end
        self.user = user
        self.gameElement = gameElement
        self.actionName = "Action"
        self.previousUpdate = previousUpdate

    def __str__(self):
        if self.previousUpdate is None:
            return str(self.actionName) + ";" + str(self.user.name) + ";" + str(self.gameElement) + ";" + str(self.start) + ";" + str(self.end) + "; ;"
        else :
            return str(self.actionName) + ";" + str(self.user.name) + ";" + str(self.gameElement) + ";" + str(self.start) + ";" + str(self.end) + ";" + str(self.start-self.previousUpdate) + ";"


    def __lt__(self, other):
        if self.start < other.start:
            return True
        elif self.start > other.start:
            return False
        else:
            return self.end < other.end

class Pause(Observation):
    def __init__(self, start: datetime, end: datetime, user: User, gameElement: GameElement, op1: Operation, op2: Operation, previousUpdate: datetime):
        """

        :param start: A datetime at the start of the Action
        :param end:  A date time at the end of the Action
        :param user: The user that performed the Action
        :param gameElement: The game element that the user had when the performed the Action
        :param op1: The first operation
        :param op2: The second operation
        """
        super().__init__(start, end, user, gameElement, previousUpdate)
        self.op1 = op1
        self.op2 = op2
        self.actionName = "Pause"

    def __str__(self):
        return super().__str__() + "Duration:" + str(self.getPauseDuration())+ ";" + str(self.op1) + ";" + str(self.op2)

    def getPauseDuration(self) -> timedelta:
        """
        Get the duration of the Pause Action
        :return: The timedelta between the start and the end
        """
        return self.end - self.start

class AttemptQuiz(Observation):
    def __init__(self, start: datetime, end: datetime, user: User, gameElement: GameElement, quizId: str, attemptNumber: int, previousUpdate: datetime):
        """

        :param start: A datetime at the start of the Action
        :param end:  A date time at the end of the Action
        :param user: The user that performed the Action
        :param gameElement: The game element that the user had when the performed the Action
        :param quizId: The Id of the attempted quiz
        :param attemptNumber: The attempt number
        """
        super().__init__(start, end, user, gameElement, previousUpdate)
        self.quizId = quizId
        self.attemptNumber = attemptNumber
        self.actionName = "AttemptQuiz"

    def __str__(self):
        return super().__str__() + "QuizId:"+ self.quizId + ";" + "Attempt:" + str(self.attemptNumber)

class ResumeQuizAttempt(Observation):
    def __init__(self, start: datetime, end: datetime, user: User, gameElement: GameElement, quizId: str, attemptNumber: int, previousUpdate: datetime):
        """

        :param start: A datetime at the start of the Action
        :param end:  A date time at the end of the Action
        :param user: The user that performed the Action
        :param gameElement: The game element that the user had when the performed the Action
        :param quizId: The Id of the attempted quiz
        :param attemptNumber: The attempt number
        """
        super().__init__(start, end, user, gameElement, previousUpdate)
        self.quizId = quizId
        self.attemptNumber = attemptNumber
        self.actionName = "ResumeQuizAttempt"

    def __str__(self):
        return super().__str__() + "QuizId:"+ self.quizId + ";" + "Attempt:" + str(self.attemptNumber)

class CompleteQuestion(Observation):
    def __init__(self, start: datetime, end: datetime, user: User, gameElement: GameElement, previousUpdate: datetime, success: bool = True):
        """

        :param start: A datetime at the start of the Action
        :param end:  A date time at the end of the Action
        :param user: The user that performed the Action
        :param gameElement: The game element that the user had when the performed the Action
        :param success: Whether the question was answered correctly or not
        """
        super().__init__(start, end, user, gameElement, previousUpdate)
        self.success = success
        self.actionName = "CompleteQuestion"

    def __str__(self):
        return super().__str__() + "Success:" + str(self.success)

class CompleteQuiz(Observation):
    def __init__(self, start: datetime, end: datetime, user: User, gameElement: GameElement, quizId: str, attemptNumber: int, previousUpdate: datetime, interruption: bool = False, success: bool = True):

        """

        :param start: A datetime at the start of the Action
        :param end:  A date time at the end of the Action
        :param user: The user that performed the Action
        :param gameElement: The game element that the user had when the performed the Action
        :param quizId: The Id of the attempted quiz
        :param attemptNumber: The attempt number
        :param interruption: Indicate whether the quiz was interrupted or not
        :param success: Indicate whether the student validated the quiz or not
        """
        super().__init__(start, end, user, gameElement, previousUpdate)
        self.quizId = quizId
        self.attemptNumber = attemptNumber
        self.interruption = interruption
        self.success = success
        self.actionName = "CompleteQuiz"


    def __str__(self):
        return super().__str__() +  "QuizId:"+ self.quizId + ";" + "Attempt:" + str(self.attemptNumber) + ";Interruption:" + str(self.interruption) + ";Success:" + str(self.success)

class RestartQuiz(Observation):
    def __init__(self, start: datetime, end: datetime, user: User, gameElement: GameElement, quizId: str, attemptNumber: int, previousUpdate: datetime, afterSuccess: bool = False,
                 inCurrentLesson: bool = True,
                 interruptCurrentLesson: bool = False):
        """

        :param start: A datetime at the start of the Action
        :param end:  A date time at the end of the Action
        :param user: The user that performed the Action
        :param gameElement: The game element that the user had when the performed the Action
        :param quizId: The Id of the attempted quiz
        :param attemptNumber: The attempt number
        :param afterSuccess: Indicate whether the quiz was restarted after a successful quiz or not
        :param inCurrentLesson: Indicate whether the quiz was restarted during a later lesson
        :param interruptCurrentLesson: Indicate whether the quiz was restarted before the current lesson was completed
        """
        super().__init__(start, end, user, gameElement, previousUpdate)
        self.quizId = quizId
        self.attemptNumber = attemptNumber
        self.afterSuccess = afterSuccess
        self.inCurrentLesson = inCurrentLesson
        self.interruptCurrentLesson = interruptCurrentLesson
        self.actionName = "RestartQuiz"

    def __str__(self):
        return super().__str__() +  "QuizId:"+ self.quizId + ";" + "Attempt:" + str(self.attemptNumber) + ";AfterSuccess:" + str(self.afterSuccess) + ";InCurrentLesson:" + str(self.inCurrentLesson) + ";InterruptCurrentLesson:" + str(self.interruptCurrentLesson)

class AbandonQuiz(Observation):
    def __init__(self, start: datetime, end: datetime, user: User, gameElement: GameElement, quizId: str,
                 attemptNumber: int, numberOfQuestions: int, correctQuestions: int, previousUpdate: datetime):
        """

        :param start: A datetime at the start of the Action
        :param end:  A date time at the end of the Action
        :param user: The user that performed the Action
        :param gameElement: The game element that the user had when the performed the Action
        :param quizId: The Id of the attempted quiz
        :param attemptNumber: The attempt number
        :param numberOfQuestions: The number of attempted questions
        :param correctQuestions: The number of correct questions
        """
        super().__init__(start, end, user, gameElement, previousUpdate)
        self.quizId = quizId
        self.attemptNumber = attemptNumber
        self.numberOfQuestions = numberOfQuestions
        self.correctQuestions = correctQuestions
        self.actionName = "AbandonQuiz"

    def __str__(self):
        return super().__str__() + "QuizId:" + self.quizId + ";Attempt:" + str(self.attemptNumber) + ";NumberOfQuestions:" + str(self.numberOfQuestions) + ";CorrectionQuestions" + str(self.correctQuestions)

class StartLesson(Observation):
    def __init__(self, start: datetime, end: datetime, user: User, gameElement: GameElement, lessonId: str, previousUpdate: datetime):
        """

        :param start: A datetime at the start of the Action
        :param end:  A date time at the end of the Action
        :param user: The user that performed the Action
        :param gameElement: The game element that the user had when the performed the Action
        :param lessonId: The Id of the lesson started
        """
        super().__init__(start, end, user, gameElement, previousUpdate)
        self.lessonId = lessonId
        self.actionName = "StartLesson"

    def __str__(self):
        return super().__str__() +  "LessonId:"+ self.lessonId

class CompleteLesson(Observation):
    def __init__(self, start: datetime, end: datetime, user: User, gameElement: GameElement, lessonId: str, previousUpdate: datetime, success: bool = True):
        """

        :param start: A datetime at the start of the Action
        :param end:  A date time at the end of the Action
        :param user: The user that performed the Action
        :param gameElement: The game element that the user had when the performed the Action
        :param lessonId: The Id of the lesson started
        :param success: Has the lesson been completed fully or not
        """
        super().__init__(start, end, user, gameElement, previousUpdate)
        self.lessonId = lessonId
        self.success = success
        self.actionName = "CompleteLesson"

    def __str__(self):
        return super().__str__() +  "LessonId:"+ self.lessonId + ";Success:" + str(self.success)